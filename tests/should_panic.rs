#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;

use potatos::qemu::{exit_qemu, QemuExitCode};
use potatos::{serial_println, Testable};

#[no_mangle]
pub extern "C" fn _start() -> ! {
    expected_panic_works.run();
    serial_println!("[test did not panic]");
    exit_qemu(QemuExitCode::Failed);

    loop {}
}

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    serial_println!("[ok]");
    exit_qemu(QemuExitCode::Success);
    loop {}
}

fn expected_panic_works() {
    panic!("Aw, fudge!");
}
