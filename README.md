# Minimal installation

1. Install cargo
2. Install nightly toolchain for rust
3. Install rust-src component for rust
4. Install bootimage with `cargo install bootimage`
5. Install qemu for x86 architecture, "qemu-system-x86"
