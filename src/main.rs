#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(potatos::test_runner)]
#![reexport_test_harness_main = "test_main"]

use core::panic::PanicInfo;
use potatos::{init, interrupts, println};

fn main() {
    println!("Hello World{}", "!");

    interrupts::trigger_stack_overflow();
    interrupts::trigger_page_fault();
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    init();

    #[cfg(test)]
    test_main();

    main();
    println!("We did not crash!");
    loop {}
}

#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    loop {}
}

#[cfg(test)]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    potatos::test_panic_handler(info)
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
